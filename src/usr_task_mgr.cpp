/***************************************************************************
 *File name:
 *   usr_task_mgr.cpp
 *
 *File brief:
 *   Manger all of the tasks.
 *
 *File time: 
 *   2020.05.11
 *
 **************************************************************************/

#include "usr_includes.hpp"

TaskMgr_Cls::TaskMgr_Cls()
{
	;
}

TaskMgr_Cls::~TaskMgr_Cls()
{
	;
}



Res_t TaskMgr_Cls::TasksStart()
{
	Res_t tRes = RES_FAILURE;
		
	m_iImgWidthInPixels  = IMG_WIDTH;
	m_iImgHeightInPixels = IMG_HEIGHT;


	tRes = MsgMgr_Initialize();

	int32_t iTaskImgSensorMsgId = 0;
	tRes = MsgMgr_MessageQueueCreate(iTaskImgSensorMsgId);

	int32_t iTaskDispMsgId      = 0;
	tRes = MsgMgr_MessageQueueCreate(iTaskDispMsgId);

	m_clsTaskImgSensor.TaskCfg(IMG_WIDTH,IMG_HEIGHT,iTaskDispMsgId,iTaskImgSensorMsgId); 
	m_clsTaskDisp.TaskCfg(IMG_WIDTH,IMG_HEIGHT,iTaskDispMsgId, iTaskImgSensorMsgId); 

	
	m_clsTaskImgSensor.TaskCreate(TASK_IMG_SENSOR_STK_SIZE,TASK_IMG_SENSOR_PRIORITY); // "ImgSensorTask"
	m_clsTaskDisp.TaskCreate(TASK_DSIP_STK_SIZE,TASK_DSIP_PRIORITY);                  // "DispTask"

	m_clsTaskImgSensor.TaskService();
	m_clsTaskDisp.TaskService();

	
	
	m_clsTaskImgSensor.TaskJoin();
	m_clsTaskDisp.TaskJoin();

	tRes = MsgMgr_MessageQueueDestroy(iTaskImgSensorMsgId);
    tRes = MsgMgr_MessageQueueDestroy(iTaskDispMsgId);

	return tRes;
	
}




