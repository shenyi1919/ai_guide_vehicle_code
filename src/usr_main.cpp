/***************************************************************************
 *File name:
 *   usr_main.cpp
 *
 *File brief:
 *   App main file.
 *
 *File time: 
 *   2020.05.11
 *
 **************************************************************************/
#include "usr_includes.hpp"


/***************************************************************************
 * Brief:
 *   This function configures, creates and starts the various tasks involved
 *   in the forward collision warning application.
 *
 * Param:
 *   
 * Return:
 *   int
 *
 **************************************************************************/

TaskMgr_Cls TaskManager;

int main(int argc, char** argv)
{
	unsigned int iRet = 0;
	
	TaskManager.TasksStart();

	return iRet;
}

