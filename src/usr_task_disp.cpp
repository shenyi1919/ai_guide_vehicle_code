/***************************************************************************
 *File name:
 *   usr_task_disp.cpp
 *
 *File brief:
 *   disp the runtime img.
 *
 *File time: 
 *   2020.05.11
 *
 **************************************************************************/

#include "usr_includes.hpp"



TaskDisp_Cls::TaskDisp_Cls()
{
	 ;
}

TaskDisp_Cls::~TaskDisp_Cls()
{
	 ;
}



Res_t TaskDisp_Cls::TaskCfg(unsigned int _iImgWidth,unsigned int _iImgHeight,unsigned int _iRecvMsgID,unsigned int _iSendMsgID)
{
	m_iRecvMsgID = _iRecvMsgID;
	m_iSendMsgID = _iSendMsgID;

	m_iImgWidth  = _iImgWidth;
	m_iImgHeight = _iImgHeight;

	return RES_SUCCESS;
}

Res_t TaskDisp_Cls::TaskOpen(void)
{
	m_clsFrmDisp.Init(IMG_WIDTH, IMG_HEIGHT, io::IO_DATA_DEPTH_08, io::IO_DATA_CH3);
	return RES_SUCCESS;
}


Res_t TaskDisp_Cls::TaskService(void)
{
	Res_t tRes = RES_FAILURE;

	MessageBase* pstRecvMsg      = NULL;
	TransImgMsg* pstImgMsg       = NULL;
    int32_t      iMsgSizeInBytes = 0;

	MsgMgr_MessageSend(m_iSendMsgID,&m_stImageMsg, sizeof(TransImgMsg), 1);  // start sensor task

	while(1)
	{
		tRes = MsgMgr_MessageReceive(m_iRecvMsgID,pstRecvMsg,iMsgSizeInBytes,0xFFFFFFFF); // 0xFFFFFFFF wait forever

		if(tRes == RES_SUCCESS)
		{
			pstImgMsg = (TransImgMsg*)pstRecvMsg;
			m_stImageMsg.m_clsTransUMat = pstImgMsg->m_clsTransUMat;

			m_clsFrmDisp.PutFrame(m_stImageMsg.m_clsTransUMat);

			m_stImageMsg.mMessageId     = TRANS_IMG_MSG_DISP_TO_SENSOR;
			m_stImageMsg.mCorrelationId = 0;
			m_stImageMsg.mSenderId      = 0;
			m_stImageMsg.mPriority      = 1;

			tRes = MsgMgr_MessageSend(m_iSendMsgID,&m_stImageMsg, sizeof(TransImgMsg), 1);
		}
	}

	return tRes;
}





