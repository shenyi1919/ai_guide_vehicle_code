/***************************************************************************
 *File name:
 *   usr_task_img_sensor.cpp
 *
 *File brief:
 *   Image input from sersor.
 *
 *File time: 
 *   2020.05.11
 *
 **************************************************************************/
#include "usr_includes.hpp"


TaskImgSensor_Cls::TaskImgSensor_Cls()
{
	 m_cCamSwapFlag = OFF;
}

TaskImgSensor_Cls::~TaskImgSensor_Cls()
{
	;
}


Res_t TaskImgSensor_Cls::TaskCfg(unsigned int _iImgWidth,unsigned int _iImgHeight,unsigned int _iRecvMsgID,unsigned int _iSendMsgID)
{
	m_iRecvMsgID = _iRecvMsgID;
	m_iSendMsgID = _iSendMsgID;

	m_iImgWidth  = _iImgWidth;
	m_iImgHeight = _iImgHeight;

	return RES_SUCCESS;
}


#define DDR_BUFFER_X_3  3
Res_t TaskImgSensor_Cls::TaskOpen(void)
{
	Res_t tRes = RES_FAILURE;

	tRes = sdi::Initialize(0);

	if(tRes == RES_SUCCESS)
	{
		tRes = m_stCam1.m_clsCamGrabber.ProcessSet(gpGraph_mipi_simple, &gGraphMetadata_mipi_simple, kmem_srec_mipi_simple, sequencer_srec_mipi_simple);                               
	}

	SDI_ImageDescriptor stCam1FrmDesc;

	if(tRes == RES_SUCCESS)
	{
		m_stCam1.m_pclsCamFdma = (sdi_FdmaIO*)m_stCam1.m_clsCamGrabber.IoGet(SEQ_OTHRIX_FDMA);  
		stCam1FrmDesc  = SDI_ImageDescriptor(IMG_WIDTH,IMG_HEIGHT,RGB888);

		tRes = m_stCam1.m_pclsCamFdma->DdrBufferDescSet(0,stCam1FrmDesc);
	}

	if(tRes == RES_SUCCESS)
	{
		tRes = m_stCam1.m_pclsCamFdma->DdrBuffersAlloc(DDR_BUFFER_X_3);
	}

	

	if(tRes == RES_SUCCESS)
	{
		tRes = m_stCam2.m_clsCamGrabber.ProcessSet(gpGraph_sony_dualexp, &gGraphMetadata_sony_dualexp, kmem_srec_sony_dualexp, sequencer_srec_sony_dualexp);                               
	}

	SDI_ImageDescriptor stCam2FrmDesc;
	
	if(tRes == RES_SUCCESS)
	{
		m_stCam2.m_pclsCamFdma = (sdi_FdmaIO*)m_stCam2.m_clsCamGrabber.IoGet(SEQ_OTHRIX_FDMA); 
		stCam2FrmDesc  = SDI_ImageDescriptor(IMG_WIDTH,IMG_HEIGHT,RGB888);

		tRes = m_stCam2.m_pclsCamFdma->DdrBufferDescSet(0,stCam2FrmDesc);
	}

	if(tRes == RES_SUCCESS)
	{
		tRes = m_stCam2.m_pclsCamFdma->DdrBuffersAlloc(DDR_BUFFER_X_3);
	}

	return tRes;
}


#define TRANS_IMG_MSG_SENSOR_OFFSET 1
Res_t TaskImgSensor_Cls::TaskService(void)
{
	Res_t tRes = RES_FAILURE;

	MessageBase* pstRecvMsg      = NULL;
    int32_t      iMsgSizeInBytes = 0;
	CamInfo_t*   pstCam          = NULL;

	while(1)
	{
		tRes = MsgMgr_MessageReceive(m_iRecvMsgID,pstRecvMsg,iMsgSizeInBytes,0xFFFFFFFF); // 0xFFFFFFFF wait forever

		if(m_cCamSwapFlag == OFF)
		{
			pstCam = &m_stCam1;
			m_cCamSwapFlag = ON;
		}
		else
		{
			pstCam = &m_stCam2;
			m_cCamSwapFlag = OFF;	
		}

		tRes = pstCam->m_clsCamGrabber.PreStart();
		
		if(tRes == RES_SUCCESS)
		{
			tRes = pstCam->m_clsCamGrabber.Start();
		}

		if(tRes == RES_SUCCESS)
		{
			tRes = pstCam->m_clsCamGrabber.FwCheck();
		}

		if(tRes == RES_SUCCESS)
		{
			pstCam->m_stCamFrame = pstCam->m_clsCamGrabber.FramePop();
		}

		if(!pstCam->m_stCamFrame.mUMat.empty())
		{
			m_stImageMsg.m_clsTransUMat = pstCam->m_stCamFrame.mUMat;
			m_stImageMsg.mMessageId     = TRANS_IMG_MSG_SENSOR_TO_DISP;
			m_stImageMsg.mCorrelationId = 0;
			m_stImageMsg.mSenderId      = 0;
			m_stImageMsg.mPriority      = 1;

			tRes = MsgMgr_MessageSend(m_iSendMsgID,&m_stImageMsg, sizeof(TransImgMsg), 1);

			if(tRes == RES_SUCCESS)
		    {
				tRes = pstCam->m_clsCamGrabber.FramePush(pstCam->m_stCamFrame);
			}
		}
		else
		{
			tRes = RES_FAILURE;
		}

		if(tRes == RES_SUCCESS)
		{
			tRes = pstCam->m_clsCamGrabber.Stop();
		}

		if(tRes == RES_SUCCESS)
		{
			tRes = pstCam->m_clsCamGrabber.Release();
		}
	}

	return tRes;
}









