/***************************************************************************
 *File name:
 *   usr_task_type.cpp
 *
 *File brief:
 *   Task level class define.
 *
 *File time: 
 *   2020.05.11
 *
 **************************************************************************/
#include "usr_includes.hpp"

sem_t TaskBase_Cls::m_tTaskSafetySem;

TaskBase_Cls::TaskBase_Cls()
{
	sem_init(&m_tTaskSafetySem, 0, 1);
}

TaskBase_Cls::~TaskBase_Cls()
{
	;
}

void* TaskBase_Cls::TaskSrtRoutine(void* _pVoid)
{
	TaskBase_Cls* const pTask = (TaskBase_Cls*)_pVoid;

    pTask->TaskOpen();
    pTask->TaskService();
    //pTask->TaskClose();

    return 0;
}


Res_t TaskBase_Cls::TaskCreate(unsigned int _iStkSizeInBytes,unsigned int _iTaskPriority)
{
	Res_t          tRet          = RES_FAILURE;
	pthread_attr_t tThreadAttr;
	pthread_t*     ptThread;
	sched_param    stSchedParam;
	size_t         tDefStkSize   = 0;

	tRet = sem_wait(&m_tTaskSafetySem);

	if(tRet == RES_SUCCESS)
	{
		tRet = RES_FAILURE;

        tRet = pthread_attr_init(&tThreadAttr);

		if(tRet == RES_SUCCESS)
		{
			tRet = pthread_attr_setdetachstate(&tThreadAttr, PTHREAD_CREATE_JOINABLE);
		}

		if(tRet == RES_SUCCESS)
		{
			tRet = pthread_attr_getschedparam(&tThreadAttr, &stSchedParam);
		}

		if(tRet == RES_SUCCESS)
		{
			stSchedParam.sched_priority += _iTaskPriority;
			tRet = pthread_attr_setschedparam(&tThreadAttr, &stSchedParam);
		}

        if(tRet == RES_SUCCESS)
        {
          	tRet = pthread_attr_getstacksize(&tThreadAttr, &tDefStkSize);
        }

        if(tRet == RES_SUCCESS)
        {
          	tRet = pthread_attr_setstacksize(&tThreadAttr, (tDefStkSize > _iStkSizeInBytes) ? tDefStkSize : _iStkSizeInBytes);
        }

		if(tRet == RES_SUCCESS)
        {
        	ptThread = &m_tThread;
          	tRet = pthread_create(ptThread, NULL, TaskBase_Cls::TaskSrtRoutine, this);
        }
		
	}

	sem_post(&m_tTaskSafetySem);

	return tRet;
}

Res_t TaskBase_Cls::TaskJoin(void)
{
	Res_t tRet = RES_FAILURE;

	tRet = sem_wait(&m_tTaskSafetySem);

	if(tRet == RES_SUCCESS)
	{
		tRet = pthread_join(m_tThread, NULL);
	}

	sem_post(&m_tTaskSafetySem);
	
	return tRet;
}






































 

