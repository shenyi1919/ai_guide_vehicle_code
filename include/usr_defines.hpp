/***************************************************************************
 *File name:
 *   usr_defines.hpp
 *
 *File brief:
 *   Includes all predefine and setting key.
 *
 *File time: 2020.05.11
 *
 **************************************************************************/
#ifndef _USR_DEFINES_HPP_
#define _USR_DEFINES_HPP_


#define IMG_WIDTH   720
#define IMG_HEIGHT  480



#define TASK_DSIP_STK_SIZE          4096
#define TASK_DSIP_PRIORITY          19

#define TASK_IMG_SENSOR_STK_SIZE    4096
#define TASK_IMG_SENSOR_PRIORITY    18

#endif





