/***************************************************************************
 *File name:
 *   usr_task_mgr.hpp
 *
 *File brief:
 *   Manger all of the tasks.
 *
 *File time: 
 *   2020.05.11
 *
 **************************************************************************/


#ifndef _USR_TASK_MGR_HPP_
#define _USR_TASK_MGR_HPP_


class TaskMgr_Cls
{
public:
	TaskMgr_Cls();
	virtual ~TaskMgr_Cls();

	Res_t TasksStart();
	

private:
	unsigned int      m_iImgWidthInPixels;
	unsigned int      m_iImgHeightInPixels;
	
	TaskDisp_Cls      m_clsTaskDisp;
	TaskImgSensor_Cls m_clsTaskImgSensor;


private:
	TaskMgr_Cls( const TaskMgr_Cls& );              // Do not implement
   	TaskMgr_Cls& operator=( const TaskMgr_Cls& ); // Do not implement

};




#endif 

