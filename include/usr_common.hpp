/***************************************************************************
 *File name:
 *   usr_common.hpp
 *
 *File brief:
 *   Includes all common interface and define.
 *
 *File time: 
 *   2020.05.11
 *
 **************************************************************************/
 
#ifndef _USR_COMMON_HPP_
#define _USR_COMMON_HPP_

typedef int Res_t;
#define RES_SUCCESS  0
#define RES_FAILURE -1

#define ON           1
#define OFF          0



#endif 

