/***************************************************************************
 *File name:
 *   usr_task_disp.hpp
 *
 *File brief:
 *   disp the runtime img.
 *
 *File time: 
 *   2020.05.11
 *
 **************************************************************************/

#ifndef _USR_TASK_DISP_HPP_
#define _USR_TASK_DISP_HPP_

class TaskDisp_Cls : public TaskBase_Cls
{

public:
	TaskDisp_Cls();
	virtual ~TaskDisp_Cls();

	Res_t TaskCfg(unsigned int _iImgWidth,unsigned int _iImgHeight,unsigned int _iRecvMsgID,unsigned int _iSendMsgID);

private:
	unsigned int m_iRecvMsgID;
	unsigned int m_iSendMsgID;

	unsigned int m_iImgWidth;
	unsigned int m_iImgHeight;

	io::FrameOutputV234Fb m_clsFrmDisp;
	

public:
	Res_t TaskOpen(void);
    Res_t TaskService(void); 
    Res_t TaskClose(void);

private:
	Res_t DispHandle(void);

private:
	TaskDisp_Cls( const TaskDisp_Cls& );              // Do not implement
   	TaskDisp_Cls& operator=( const TaskDisp_Cls& ); // Do not implement
};


#endif

