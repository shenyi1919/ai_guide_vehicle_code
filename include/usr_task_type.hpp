/***************************************************************************
 *File name:
 *   usr_task_type.hpp
 *
 *File brief:
 *   Task level class define.
 *
 *File time: 
 *   2020.05.11
 *
 **************************************************************************/

#ifndef _USR_TASK_TYPE_HPP_
#define _USR_TASK_TYPE_HPP_

struct TransImgMsg : public MessageBase
{
	vsdk::UMat m_clsTransUMat;
};

#define TRANS_IMG_MSG_BASE_ID 0
#define TRANS_IMG_MSG_SENSOR_TO_DISP (TRANS_IMG_MSG_BASE_ID + 1)
#define TRANS_IMG_MSG_DISP_TO_SENSOR (TRANS_IMG_MSG_BASE_ID + 2)





#define TASK_NAME_SIZE 30
class TaskBase_Cls
{
public:
   /************************************************************************** 
    * Cnstructor
    **************************************************************************/
   TaskBase_Cls();



   /***************************************************************************
    * Dtructor
    **************************************************************************/
   virtual ~TaskBase_Cls();



   /***************************************************************************	
    * This function creates and activates a new thread.
    * It is equivalent to calling "Init" and "Activate".
    *
    *
    * \param  _iStackSizeInBytes - [in] Size in bytes of the tasks stack
    * \param  _iTaskPriority     - [in] Specifies a priority value between 0 and 255.
    *                              The lower the numeric value, the higher the tasks priority.
    * \param  _acTaskName        - [in] Pointer to a 31 character name for the task.
    *                              This name is shared by the task and its default message queue.
    *                              For name registration to succeed this name must be unique.
    *
    *
    * \return
    *    - #RES_SUCCESS
    *    - #RES_FAILURE
    **************************************************************************/
	Res_t TaskCreate(unsigned int _iStkSizeInBytes,unsigned int _iTaskPriority);


   /***************************************************************************
    * This function waits for the thread to finish.
    *
    * \return
    *    - #RES_SUCCESS
    *    - #RES_FAILURE
   **************************************************************************/
   Res_t TaskJoin(void);

protected:
	TransImgMsg       m_stImageMsg;

private:
	static sem_t      m_tTaskSafetySem;
	pthread_t         m_tThread;


private: // Pure Virtual hence you CANNOT instantiate this class
   virtual Res_t TaskOpen(void)    {return RES_SUCCESS;}
   virtual Res_t TaskService(void) = 0; 
  // virtual Res_t TaskClose(void)   {return RES_SUCCESS;}


private:
   unsigned int         iStackSizeInBytes;
   unsigned int         iPriority;          // Lower the value; higher the priority
   char                 mName[TASK_NAME_SIZE];

   static void* TaskSrtRoutine(void* _pVoid);      // static for what? 


private:
   TaskBase_Cls( const TaskBase_Cls& );              // Do not implement
   TaskBase_Cls& operator=( const TaskBase_Cls& ); // Do not implement
};



#endif 



