/***************************************************************************
 *File name:
 *   usr_includes.hpp
 *
 *File brief:
 *   Includes all useful head files.
 *
 *File time: 2020.05.11
 *
 **************************************************************************/
 
#ifndef _USR_INCLUDES_HPP_
#define _USR_INCLUDES_HPP_

// std
#include <stdlib.h>
#include <string.h>
#include <stdio.h>


// nxp
#include "umat.hpp"
#include "sumat.hpp"
#include "sdi.hpp"
#include "communications.hpp"
#include "mipi_simple_c.h"
#include "sony_dualexp_c.h"
#include "sdi.hpp"
#include "sdi_io.hpp"
#include "sdi_types.hpp"
#include "sdi_graph.hpp"
#include "seq_graph.h"
#include "frame_output_display.h"
#include "frame_output_v234fb.h"


// linux
#include <semaphore.h>
#include <pthread.h>
#include <sys/types.h>



// usr
#include "usr_defines.hpp"
#include "usr_common.hpp"

#include "usr_task_type.hpp"
#include "usr_task_img_sensor.hpp"
#include "usr_task_disp.hpp"
#include "usr_task_mgr.hpp"





#endif 

