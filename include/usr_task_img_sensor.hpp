/***************************************************************************
 *File name:
 *   usr_task_img_sensor.hpp
 *
 *File brief:
 *   Image input from sersor.
 *
 *File time: 
 *   2020.05.11
 *
 **************************************************************************/
#ifndef _USR_TASK_IMG_SENSOR_HPP_
#define _USR_TASK_IMG_SENSOR_HPP_ 

typedef struct CAM_INFO{
	sdi_grabber    m_clsCamGrabber;
	sdi_FdmaIO*    m_pclsCamFdma;
	SDI_Frame      m_stCamFrame;
}CamInfo_t;




class TaskImgSensor_Cls : public TaskBase_Cls
{

public:
	TaskImgSensor_Cls();
	virtual ~TaskImgSensor_Cls();

	Res_t TaskCfg(unsigned int _iImgWidth,unsigned int _iImgHeight,unsigned int _iRecvMsgID,unsigned int _iSendMsgID);

private:
	//unsigned int   m_iRecvMsgID;
	int32_t  m_iRecvMsgID;
	unsigned int   m_iSendMsgID;

	unsigned int   m_iImgWidth;
	unsigned int   m_iImgHeight;

	CamInfo_t      m_stCam1;
	CamInfo_t      m_stCam2;

	unsigned char  m_cCamSwapFlag;
	

public:
	Res_t TaskOpen(void);
    Res_t TaskService(void); 
    Res_t TaskClose(void);

private:
	Res_t ImgSensorHandle(void);

private:
	TaskImgSensor_Cls( const TaskImgSensor_Cls& );              // Do not implement
   	TaskImgSensor_Cls& operator=( const TaskImgSensor_Cls& ); // Do not implement
};



#endif 




